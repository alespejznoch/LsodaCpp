    /*
     *  This a model from the article:
     *  Calcium spiking.
     *
     *    Meyer T, Stryer L
     *    Annu Rev Biophys Biophys Chem
     *    1991:20:153-74
     *    http://www.ncbi.nlm.nih.gov/pubmed/1867714
     *
     *    The IP3-Ca2+ Crosscoupling Model (ICC) is reviewed
     *    by Meyer and Stryer in 1991, originally from Meyer and Stryer, 1988.
     *    PMID - http://www.ncbi.nlm.nih.gov/pubmed/2455890
     *
     *    Parameters refer to figures 5 and 6 of the article
     *    which were reproduced by using Copasi 4.5 (Build 30).
     *
     *    Species CaI and IP3 are buffered to 1% and 50% percent, respectively.
     *
     *    This model originates from BioModels Database:
     *    A Database of Annotated Published Models.
     *    It is copyright (c) 2005-2009 The BioModels Team.
     *
     *   Source: https://wwwdev.ebi.ac.uk/biomodels/BIOMD0000000224
     *
     *   Expected output:
     *
     *  [0]=0.1000000015	[1]=0.0500000007	[2]=1100.0000000000	[3]=0.0000000000
     *  [0]=8.2326630087	[1]=0.1082849969	[2]=1091.8673369928	[3]=-0.0199873993
     *  [0]=12.9804108143	[1]=0.1601947677	[2]=1087.1195891872	[3]=-0.0398448491
     *  [0]=20.5384818194	[1]=0.2084778982	[2]=1079.5615181821	[3]=-0.0590027688
     *  [0]=42.8320193428	[1]=0.2842635499	[2]=1057.2679806587	[3]=-0.0685276407
     *  [0]=124.9283130615	[1]=0.4482095698	[2]=975.1716869400	[3]=0.4040104198
     *  [0]=135.1947780710	[1]=0.5950193960	[2]=964.9052219305	[3]=0.9830430899
     *  [0]=105.0493072489	[1]=0.6207943952	[2]=995.0506927526	[3]=0.9883785962
     *  [0]=77.3812199453	[1]=0.5849898053	[2]=1022.7187800561	[3]=0.9789899535
     *  [0]=54.0114278326	[1]=0.5195611755	[2]=1046.0885721689	[3]=0.9641559339
     *  [0]=34.1489750554	[1]=0.4368644602	[2]=1065.9510249461	[3]=0.9459277704
     *  [0]=18.1128803192	[1]=0.3426876716	[2]=1081.9871196823	[3]=0.9262610886
     *  [0]=9.3525003845	[1]=0.2536219757	[2]=1090.7474996170	[3]=0.9062914961
     *  [0]=6.1088175928	[1]=0.1935130327	[2]=1093.9911824087	[3]=0.8862949340
     *  [0]=4.8679968314	[1]=0.1606719191	[2]=1095.2320031701	[3]=0.8662959835
     *  [0]=4.4212672588	[1]=0.1444042264	[2]=1095.6787327427	[3]=0.8462966190
     *
     */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <sys/time.h>

#include "LsodaCpp.hpp"

struct timespec diff(const struct timespec &start, const struct timespec &end) {
    struct timespec temp;
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

template <typename RealType> class Functor {
public:
    void initialize(int *const &lens, char const ** const &tree,
                    RealType **const &constants, int **const &variables) {
        stack = (RealType*)malloc(lens[0] * sizeof(RealType));
        this->lens = lens;
        this->tree = tree;
        this->constants = constants;
        this->variables = variables;
    }

    void operator()(const int &dims, const RealType &t, RealType *const &y, RealType *ydot) {
        for (int dim = 0; dim < dims; ++dim) {

            int stackPos = 0;
            char const *localTree = tree[dim];
            RealType *localConstants = constants[dim];
            int *localVariables = variables[dim];

            for (int i = lens[dim] - 1; i >= 0; --i) {

                switch (localTree[i]) {

                case 'C':
                    stack[stackPos] = localConstants[i];
                    break;

                case 'V':
                    stack[stackPos] = y[localVariables[i] - 1];
                    break;

                case '-':
                    stackPos -= 2;
                    stack[stackPos] = stack[stackPos + 1] - stack[stackPos];
                    break;

                case '+':
                    stackPos -= 2;
                    stack[stackPos] = stack[stackPos + 1] + stack[stackPos];
                    break;

                case '*':
                    stackPos -= 2;
                    stack[stackPos] = stack[stackPos + 1] * stack[stackPos];
                    break;

                case '/':
                    stackPos -= 2;
                    stack[stackPos] = stack[stackPos + 1] / stack[stackPos];
                    break;

                case '^':
                    stackPos -= 2;
                    stack[stackPos] = pow(stack[stackPos + 1], stack[stackPos]);
                    break;

                default:
                    fprintf(stderr, "error: undefined node");
                    ydot[dim] = 0;
                }
                ++stackPos;
            }

            ydot[dim] = stack[stackPos - 1];
        }
    }

    void destroy() { free(stack); }

private:
    RealType    *stack;
    int         *lens;
    char const **tree;
    RealType   **constants;
    int        **variables;
};


//testing executable
int main () {
    int64_t maxSteps = 186;
    double startTime = 0.f;
    double timeStep = 1.f;
    double maxRelativeError = 1E-3f;
    double maxAbsoluteError = 1E-3f;
    const int variableCount = 4;

    double initialValues[4] = {0.1f, 0.05f, 1100.f, 0.f};

    double iMaxAbsoluteErrors[variableCount];
    double iMaxRelativeErrors[variableCount];
    for (int i=0; i<variableCount; ++i) {
        iMaxRelativeErrors[i] = maxRelativeError;
        iMaxAbsoluteErrors[i] = maxAbsoluteError;
    }
    
    int lens[variableCount];
    lens[0] = 41;
    lens[1] = 23;
    lens[2] = 41;
    lens[3] = 13;

    const char * iTree[4];
    iTree[0] = "-**-CV+/*C^*VCC^+*VCCCCV/*C^*VCC+^*VCC^CC";
    iTree[1] = "-*C-C*/C+*VCC/C+CC**CVC";
    iTree[2] = "-/*C^*VCC+^*VCC^CC**-CV+/*C^*VCC^+*VCCCCV";
    iTree[3] = "-**C^*VCC-CVC";

    double iConstantsTemp[4][41] = {
            {0.000000, 0.000000, 0.000000, 0.000000, 1.000000,
             0.000000, 0.000000, 0.000000, 0.000000, 23.150000,
             0.000000, 0.000000, 0.000000, 0.500000, 4.000000,
             0.000000, 0.000000, 0.000000, 0.000000, 0.500000,
             0.500000, 4.000000, 0.010000, 0.000000, 0.000000,
             0.000000, 36.250000, 0.000000, 0.000000, 0.000000,
             0.010000, 2.000000, 0.000000, 0.000000, 0.000000,
             0.000000, 0.010000, 2.000000, 0.000000, 0.150000,
             2.000000},

            {0.000000, 0.000000, 1.100000, 0.000000, 1.000000,
             0.000000, 0.000000, 1.000000, 0.000000, 0.000000,
             0.000000, 0.010000, 1.000000, 0.000000, 1.000000,
             0.000000, 1.000000, 0.090000, 0.000000, 0.000000,
             2.000000, 0.000000, 0.500000},

            {0.000000, 0.000000, 0.000000, 36.250000, 0.000000,
             0.000000, 0.000000, 0.010000, 2.000000, 0.000000,
             0.000000, 0.000000, 0.000000, 0.010000, 2.000000,
             0.000000, 0.150000, 2.000000, 0.000000, 0.000000,
             0.000000, 1.000000, 0.000000, 0.000000, 0.000000,
             0.000000, 23.150000, 0.000000, 0.000000, 0.000000,
             0.500000, 4.000000, 0.000000, 0.000000, 0.000000,
             0.000000, 0.500000, 0.500000, 4.000000, 0.010000,
             0.000000},

            {0.000000, 0.000000, 0.000000, 1.000000, 0.000000,
             0.000000, 0.000000, 0.010000, 4.000000, 0.000000,
             1.000000, 0.000000, 0.020000} };

    int iVariablesTemp[4][41] = {
            {0, 0, 0, 0, 0, 4, 0, 0, 0, 0,
             0, 0, 2, 0, 0, 0, 0, 0, 2, 0,
             0, 0, 0, 3, 0, 0, 0, 0, 0, 1,
             0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
             0},

            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 2, 0},

            {0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
             0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 4, 0, 0, 0, 0, 0, 0, 2,
             0, 0, 0, 0, 0, 2, 0, 0, 0, 0,
             3},

            {0, 0, 0, 0, 0, 0, 1, 0, 0, 0,
             0, 4, 0} };

    double ** iConstants = (double**)malloc(variableCount * sizeof(double*));
    int   ** iVariables = (int**)malloc(variableCount * sizeof(int*));

    for (int i=0; i<variableCount; ++i) {
        iConstants[i] = (double*)malloc(41 * sizeof(double));
        iVariables[i] = (int*)malloc(41 * sizeof(int));
        memcpy(iConstants[i], iConstantsTemp[i], 41 * sizeof(double));
        memcpy(iVariables[i], iVariablesTemp[i], 41 * sizeof(int));
    }
 
    double * output = (double*)malloc(variableCount * (maxSteps+1) * sizeof(double));

    Functor<double> functor;
    struct timespec time1, time2;
    clock_gettime(CLOCK_MONOTONIC, &time1);

    LsodaCpp::SolveParameters<double> params;
    params.itol = LsodaCpp::ITOL::RTOL_SCALAR_ATOL_ARRAY;
    params.maxAbsoluteErrors = iMaxAbsoluteErrors;
    params.maxRelativeErrors = iMaxRelativeErrors;
    params.variableCount = variableCount;
    params.startTime = startTime;
    params.timeStep = timeStep;
    params.maxSteps = maxSteps;
    params.initialValues = initialValues;

    LsodaCpp::Result<double> result;
    functor.initialize(lens, iTree, iConstants, iVariables);
    result = LsodaCpp::solve(functor, params, output);
    functor.destroy();
    if (result.code != LsodaCpp::CODE::OK) {
        std::cerr << "LSODACPP ERROR: " << result.msg << std::endl;
        return static_cast<int>(result.code);
    }


    clock_gettime(CLOCK_MONOTONIC, &time2);



    for (int i=0; i<variableCount; ++i) {
        free(iVariables[i]);
        free(iConstants[i]);
    }
    free(iVariables);
    free(iConstants);


    int outStepMax = 15;
    for(int i=0; i<variableCount*(outStepMax+1); i+=4) {
        fprintf(stdout, "[0]=%.10f\t[1]=%.10f\t[2]=%.10f\t[3]=%.10f\n", output[i], output[i+1], output[i+2], output[i+3]);
    }
    printf("Computation time: %ld:%ld\n", diff(time1,time2).tv_sec, diff(time1,time2).tv_nsec);

    free(output);

    return 0;
}
